"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DBEngine;
(function (DBEngine) {
    DBEngine["SQLLITE"] = "sqlite3";
    DBEngine["MYSQL"] = "mysql";
})(DBEngine = exports.DBEngine || (exports.DBEngine = {}));
exports.parseDBEngineEnum = (val) => {
    switch (val) {
        case DBEngine.SQLLITE:
            return DBEngine.SQLLITE;
        case DBEngine.MYSQL:
            return DBEngine.MYSQL;
        default:
            throw new Error('Invalid or Unsupported DB Engine ' + val);
    }
};
//# sourceMappingURL=dbengine.enum.js.map