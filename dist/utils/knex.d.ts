import Knex from 'knex';
declare global {
    interface Window {
        require: any;
    }
}
export declare const knexClient: (config: any, databaseId: string, configuredDialect: string) => Knex;
//# sourceMappingURL=knex.d.ts.map