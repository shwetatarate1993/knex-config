"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const info_commons_1 = require("info-commons");
const knex_1 = __importDefault(require("knex"));
const constants_1 = require("../constants");
let wKnex;
wKnex = knex_1.default;
try {
    if (process.env.NODE_ENV !== 'test' && info_commons_1.APP_ENV === 'desktop' && window.require) {
        wKnex = window.require('knex');
    }
}
catch (error) {
    throw error;
}
const knexClients = new Map();
const createKnexClientByDataBaseId = (config, databaseId, configuredDialect) => {
    const dbConfig = config[databaseId][configuredDialect];
    if (dbConfig === null) {
        throw new Error('Missing configuration for databaseId : ' + databaseId);
    }
    const knexClientObj = createKnexClient(dbConfig);
    return knexClientObj;
};
const createKnexClient = (dbConfig) => {
    const dialect = dbConfig.dialect;
    const dbEngine = constants_1.parseDBEngineEnum(dialect);
    switch (dbEngine) {
        case constants_1.DBEngine.SQLLITE:
            return wKnex({
                client: dialect,
                connection: dbConfig.connection,
                useNullAsDefault: true,
            });
        case constants_1.DBEngine.MYSQL:
            return wKnex({
                client: dialect,
                connection: {
                    host: dbConfig.host,
                    user: dbConfig.username,
                    password: dbConfig.password,
                    database: dbConfig.name,
                },
                useNullAsDefault: true,
                pool: {
                    min: dbConfig.pool.min,
                    max: dbConfig.pool.max,
                },
            });
        default:
            throw new Error('Invalid or Unsupported DB Engine ' + dbEngine);
    }
};
exports.knexClient = (config, databaseId, configuredDialect) => {
    let knex = knexClients.get(databaseId);
    if (knex == null) {
        knex = createKnexClientByDataBaseId(config, databaseId, configuredDialect);
        knexClients.set(databaseId, knex);
    }
    return knex;
};
//# sourceMappingURL=knex.js.map